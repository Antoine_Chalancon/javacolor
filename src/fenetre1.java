import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JSlider;

public class fenetre1 extends JFrame {

	private JPanel contentPane;
	private JTextField textField_1;
	private JTextField textField_4;
	private JTextField textField_2;
	private JTextField textField_3;
	private JButton btnOk;
	private JSlider slider;
	private JSlider slider_1;
	private JPanel panel_2;
	private JSlider slider_2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					fenetre1 frame = new fenetre1();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public fenetre1() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.SOUTH);
		
		btnOk = new JButton("OK");
		btnOk.setName("OK");
		panel.add(btnOk);
		
		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.CENTER);
		
		panel_2 = new JPanel();
		contentPane.add(panel_2, BorderLayout.CENTER);
		panel_2.setLayout(null);
		
		textField_1 = new JTextField();
		textField_1.setBounds(12, 12, 114, 21);
		panel_2.add(textField_1);
		textField_1.setColumns(10);
		
		textField_4 = new JTextField();
		textField_4.setBounds(12, 72, 114, 21);
		panel_2.add(textField_4);
		textField_4.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(12, 136, 114, 21);
		panel_2.add(textField_2);
		textField_2.setColumns(10);
		
		slider = new JSlider();
		slider.setName("slider");
		slider.setBounds(160, 12, 200, 16);
		panel_2.add(slider);
		
		slider_1 = new JSlider();
		slider_1.setName("slider1");
		slider_1.setBounds(160, 72, 200, 16);
		panel_2.add(slider_1);
		
		slider_2 = new JSlider();
		slider_2.setName("slider2");
		slider_2.setBounds(160, 136, 200, 16);
		panel_2.add(slider_2);
		
		textField_3 = new JTextField();
		textField_3.setBounds(160, 186, 114, 21);
		panel_2.add(textField_3);
		textField_3.setColumns(10);
	}
	public JTextField getTextField_4() {
		return textField_4;
	}
	public JTextField getTextField_2() {
		return textField_2;
	}
	public JTextField getTextField_1() {
		return textField_1;
	}
	public JTextField getTextField_3() {
		return textField_3;
	}
	public JButton getBtnOk() {
		return btnOk;
	}
	public JSlider getSlider() {
		return slider;
	}
	public JSlider getSlider_1() {
		return slider_1;
	}
	public JPanel getPanel_2() {
		return panel_2;
	}
	public JSlider getSlider_2() {
		return slider_2;
	}
}
